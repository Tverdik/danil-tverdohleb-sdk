# Test-task SDK Lord of Rings

## Installation
```
npm i danil_tverdohleb-sdk
yarn add danil_tverdohleb-sdk
```
## Usage
SDK gives you access to service which can be used in development.

```js
export LordOfRingsService from 'danil_tverdohleb-sdk';

const lordOfRingsService = new LordOfRingsService('accessToken');

async function doSomething() {
  const movies = await lordOfRingsService.getAllMovies(); // array of movies
  const movies = await lordOfRingsService.getAllMovies({ option, params}); // detailed information you can see below
  
  // some operations
};
```
*token can be recieved here -> https://the-one-api.dev/sign-up

### Methods

| Method | Description |
| ------ | ------ |
| getAllBooks | returns array of all saved books |
| getBookById | returns book by specified id |
| getBookChaptersById | returns chapters of specified book |
| getAllMovies | returns array of all saved movies |
| getMovieById | returns movie by specified id |
| getMovieQuotesById | returns quotes of specified movie |
| getAllCharacters | returns array of all saved characters |
| getCharacterById | returns character by specified id |
| getCharacterQuotesById | returns quotes of specified character |
| getAllMovieQuotes | returns array of all saved movie quotes |
| getQuoteById | returns movie quote by specified id |
| getAllBookChapters | returns array of all saved chapters |
| getChapterById | returns book chapter by specified id |

### Additional options (for method which get all item category) <br><br>

#### Pagination
| Param | Data type | Description |
| ------ | ------ | ------ |
| limit | number | specifies how much items will be received |
| page | number | specifies which page will be returned |
| offset | number | specifies how much items will be skiped |

#### Sort
| Param | Data type | Description |
| ------ | ------ | ------ |
| field | string | specifies field by which it will be sorted |
| asc | boolean | specifies order of sotring - ascending/descending |

#### Filter
| Param | Data type | Description |
| ------ | ------ | ------ |
| field | string | specifies field by which filter will work, if only it specified, only items with this field will be returned, if opposite true, only items without it |
| value | String array, string, number or undefined | if only field and one value - returns items which math for it, several values - items with this values |
| operation | sting | specifies math operation which will be used (less than, greater than or equal to) |
| regex | RegExp | specifies pattern which will be used with field |
| opposite | boolean | specifies opposite action for params - regexp, field, field with value/values |

## Testing
This SDK has neccessary test for methods, you can see test/add your own only if you have access in repository.
