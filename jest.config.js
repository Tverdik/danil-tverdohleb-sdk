module.exports = {
  moduleNameMapper: {
    '^@api(.*)$': '<rootDir>/src/api/index.js',
  },
  transformIgnorePatterns: ['node_modules'],
  moduleFileExtensions: ['js', 'json'],
  rootDir: '.',
  testRegex: '.*\\.test\\.js$',
  collectCoverageFrom: ['**/*.js'],
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
};
