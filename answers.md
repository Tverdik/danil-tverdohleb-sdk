## Answers
1- Have you manually tested the SDK?  
- Yes, when I finished SDK development, I created button in html and test it for several any request, with different params, different situations. 

2 - Did you add a test suite? If so how will we use it? If not, why?  
- I did it, I added several test cases, which has more complex logic than just get some item by id. I tested functions behaviour when I use pagination, sorting and filters. And if some of this test cases will fail, it means that our functional changed and we don’t get required result.

3 - Did you use any 3rd party library? Why did you use it? What are the tradeoffs?  
- For requests I used Axios, because it very comfortable to send requests with it, I don’t need to transform my data manually after each request, like in a fetch. Also I can add token in headers by config or interceptor only one time and it will be used for every request. For testing I used Jest, jest is very famous and comfortable library which make unit testing very easy and fast, also I used nock because I needed to mock server responses. Eslint I used to create code clearer and more readable.

4 - Do you feel this SDK makes it easier to interact with the API?
- I guess yes, because we don’t need to think about which methods will be used for request or headers, it will be managed internaly,  and we get something like a facade which can create a lot of request, transform data and etc only by one function call from user side.

5 - If you had more time, what else would you add?  
- I think it can be more difficult request with more complex logic, which will help us to work more flexible and comfortable with our data. And maybe think more about testing and improve it.
Right now it works only for web, I guess I can make sdk more flexible which will allow use it on backend and make it cross-platform as a result developers on different OS won’t have problems in development.

6 - What would you change in your current SDK solution? 
- Maybe think about code structure and change it, like separate services for items, because it can increase, also maybe move private functions on the top level, where It will be used by future services. I guess it necessary to add typescript and describe types for all entities we receive. It will help us create out code more readable, understandable, also It can decrease number of tests, because we know all about types. 

7 - On a scale of 1 to 10, how would you rate this SDK? (higher is better). 
- I guess it i6.

8 - Anything else we should keep in mind when we evaluate the project?
- In my opinion we need to think about lot of thing, for example how flexible it will be for our user, how many settings he has. Maybe performance, of course right a big part of machines are fairly strong, but sometimes we don’t have enough resources, so it will be useful to think about it.