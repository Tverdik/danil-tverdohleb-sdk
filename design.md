### Main goal:

SDK which gives developers opportunity to work with API without knowing about how it works internally. Developer has list of methods which recieves different parameters and returns items which are satisfy requirements. 

### Development:
I had a technical task which describes what I need to do. The first SDK version has API and LordOfRingsService which makes requests.


### Future ideas: 
- Add typescript and describe all entities 
- Create more flexible and complex methods 
- Give opportunity to use it on backend development
