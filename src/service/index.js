import { createApi } from '@api';

export class LordOfRingsService {
  #api;
  #queryTypeHandlers = {
    pagination: this.#createPaginationQuery,
    sort: this.#createSortQuery,
    filter: this.#createFilterQuery
  };

  constructor(accessToken) {
    this.#api = createApi(accessToken);
  }

  #createPaginationQuery({ limit, offset, page }) {
    let query = '';

    if (limit >= 0) {
      query += `limit=${limit}`;
    }

    if (offset >= 0) {
      query += `${query.length > 0 ? '&' : ''}offset=${offset}`;
    }

    if (page >= 0) {
      query += `${query.length > 0 ? '&' : ''}page=${page}`;
    }

    return query;
  }

  #createSortQuery({ field, asc }) {
    return `sort=${field}:${asc ? 'asc' : 'desc'}`;
  }

  #createFilterQuery({ field, value, opposite, operation, regex }) {
    if (operation) {
      return `${field}${operation}${value}`;
    }

    if (regex) {
      return `${field}${opposite ? '!' : ''}=${regex}`;
    }

    if (!value) {
      return `${opposite ? '!' : ''}${field}`;
    }

    if (Array.isArray(value)) {
      return `${field}${opposite ? '!' : ''}=${value.join(',')}`;
    }

    return `${field}${opposite ? '!' : ''}=${value}`;
  }

  #createQueryString({ option, params } = {}) {
    if (!this.#queryTypeHandlers[option] || !Object.keys(params).length) {
      return '';
    }

    return `?${this.#queryTypeHandlers[option](params)}`;
  }

  async getAllBooks(options) {
    const { data: { docs } } = await this.#api.get(`/book${this.#createQueryString(options)}`);

    return docs;
  }

  async getBookById(id) {
    const { data: { docs: [book] } } = await this.#api.get(`/book/${id}`);

    return book;
  }

  async getBookChaptersById(id) {
    const { data: { docs } } = await this.#api.get(`/book/${id}/chapter`);

    return docs;
  }

  async getAllMovies(options) {
    const { data: { docs } } = await this.#api.get(`/movie${this.#createQueryString(options)}`);

    return docs;
  }

  async getMovieById(id) {
    const { data: { docs: [movie] } } = await this.#api.get(`/movie/${id}`);

    return movie;
  }

  async getMovieQuotesById(id) {
    const { data: { docs } } = await this.#api.get(`/movie/${id}/quote`);

    return docs;
  }

  async getAllCharacters(options) {
    const { data: { docs } } = await this.#api.get(`/character${this.#createQueryString(options)}`);

    return docs;
  }

  async getCharacterById(id) {
    const { data: { docs: [character] } } = await this.#api.get(`/character/${id}`);

    return character;
  }

  async getCharacterQuotesById(id) {
    const { data: { docs } } = await this.#api.get(`/character/${id}/quote`);

    return docs;
  }

  async getAllMovieQuotes(options) {
    const { data: { docs } } = await this.#api.get(`/quote${this.#createQueryString(options)}`);

    return docs;
  }

  async getQuoteById(id) {
    const { data: { docs: [quote] } } = await this.#api.get(`/quote/${id}`);

    return quote;
  }

  async getAllBookChapters(options) {
    const { data: { docs } } = await this.#api.get(`/chapter${this.#createQueryString(options)}`);

    return docs;
  }

  async getChapterById(id) {
    const { data: { docs: [chapter] } } = await this.#api.get(`/chapter/${id}`);

    return chapter;
  }
}
