import nock from 'nock';
import { LordOfRingsService } from './index';

const lordOfRingsService = new LordOfRingsService('_P-jj486xvNa5k3DMEQ2');

const mockedCharacters = [
  {
    birth: '',
    death: '',
    gender: 'Female',
    hair: '',
    height: '',
    name: 'Adanel',
    race: 'Human',
    realm: '',
    spouse: 'Belemir',
    wikiUrl: 'http://lotr.wikia.com//wiki/Adanel',
    _id: '5cd99d4bde30eff6ebccfbbe'
  },
  {
    birth: 'Before ,TA 1944',
    death: 'Late ,Third Age',
    gender: 'Male',
    hair: '',
    height: '',
    name: 'Beldir',
    race: 'Human',
    realm: '',
    spouse: '',
    wikiUrl: 'http://lotr.wikia.com//wiki/Beldir',
    _id: '5cd99d4bde30eff6ebccfbbf'
  },
  {
    birth: 'TA 2917',
    death: 'TA 3010',
    gender: 'Male',
    hair: '',
    height: '',
    name: 'Gandalf',
    race: 'Human',
    realm: '',
    spouse: '',
    wikiUrl: 'http://lotr.wikia.com//wiki/Gandalf',
    _id: '5cd99d4bde30eff6ebccfbc0'
  }
];

const mockedBooks = [
  { _id: '5cf5805fb53e011a64671582', name: 'The Fellowship Of The Ring' },
  { _id: '5cf58077b53e011a64671583', name: 'The Two Towers' },
  { _id: '5cf58080b53e011a64671584', name: 'The Return Of The King' }
];

const mockedMovies = [
  {
    academyAwardNominations: 7,
    academyAwardWins: 1,
    boxOfficeRevenueInMillions: 2932,
    budgetInMillions: 675,
    name: 'The Hobbit Series',
    rottenTomatoesScore: 66.33333333,
    runtimeInMinutes: 462,
    _id: '5cd95395de30eff6ebccde57'
  },
  {
    academyAwardNominations: 13,
    academyAwardWins: 4,
    boxOfficeRevenueInMillions: 871.5,
    budgetInMillions: 93,
    name: 'The Fellowship of the Ring',
    rottenTomatoesScore: 91,
    runtimeInMinutes: 178,
    _id: '5cd95395de30eff6ebccde5c'
  },
  {
    academyAwardNominations: 11,
    academyAwardWins: 11,
    boxOfficeRevenueInMillions: 1120,
    budgetInMillions: 94,
    name: 'The Return of the King',
    rottenTomatoesScore: 95,
    runtimeInMinutes: 201,
    _id: '5cd95395de30eff6ebccde5d'
  },
  {
    academyAwardNominations: 30,
    academyAwardWins: 17,
    boxOfficeRevenueInMillions: 2917,
    budgetInMillions: 281,
    name: 'The Lord of the Rings Series',
    rottenTomatoesScore: 94,
    runtimeInMinutes: 558,
    _id: '5cd95395de30eff6ebccde56'
  }
];

describe('tests for LordOfRingsService', () => {
  let scope = null;

  beforeEach(() => {
    scope = nock('https://the-one-api.dev/v2/');
  });
  describe('tests for pagination', () => {
    it('should return no more items that was specified in limit', async() => {
      scope.get('/character?limit=3').reply(200, { docs: mockedCharacters });

      const items = await lordOfRingsService.getAllCharacters({
        option: 'pagination',
        params: { limit: 3 }
      });

      expect(items).toHaveLength(3);
    });

    it('should skip as many elements as specified', async() => {
      scope.get('/book?limit=2').reply(200, { docs: mockedBooks.slice(0, 1) });
      scope.get('/book?limit=2&offset=1').reply(200, { docs: mockedBooks.slice(1, 2) });

      const [itemWithoutOffset] = await lordOfRingsService.getAllBooks({
        option: 'pagination',
        params: { limit: 2 }
      });
      const [itemWithOffset] = await lordOfRingsService.getAllBooks({
        option: 'pagination',
        params: { limit: 2, offset: 1 }
      });

      expect(itemWithoutOffset).not.toEqual(itemWithOffset);
    });

    it('should return different items on different pages', async() => {
      scope.get('/movie?limit=2&page=1').reply(200, { docs: mockedMovies.slice(0, 1) });
      scope.get('/movie?limit=2&page=2').reply(200, { docs: mockedMovies.slice(2, 3) });

      const firstPage = await lordOfRingsService.getAllMovies({
        option: 'pagination',
        params: { limit: 2, page: 1 }
      });
      const secondPage = await lordOfRingsService.getAllMovies({
        option: 'pagination',
        params: { limit: 2, page: 2 }
      });

      expect(firstPage).not.toEqual(secondPage);
    });

    it('should return an empty array if no one item was found', async() => {
      scope.get('/chapter?limit=100&page=1000').reply(200, { docs: [] });

      const items = await lordOfRingsService.getAllBookChapters({
        option: 'pagination',
        params: { limit: 100, page: 1000 }
      });

      expect(items).toHaveLength(0);
    });
  });

  describe('test for sort', () => {
    it('should return items in ascending order', async() => {
      scope.get('/movie?sort=runtimeInMinutes:asc')
        .reply(200, { docs: [...mockedMovies].sort((a, b) => a.runtimeInMinutes - b.runtimeInMinutes) });

      const [firstMovie, secondMovie] = await lordOfRingsService.getAllMovies({
        option: 'sort',
        params: { field: 'runtimeInMinutes', asc: true }
      });

      expect(firstMovie.runtimeInMinutes).toBeLessThan(secondMovie.runtimeInMinutes);
    });

    it('should return items in descending order', async() => {
      scope.get('/movie?sort=runtimeInMinutes:desc')
        .reply(200, { docs: [...mockedMovies].sort((a, b) => b.runtimeInMinutes - a.runtimeInMinutes) });

      const [firstMovie, secondMovie] = await lordOfRingsService.getAllMovies({
        option: 'sort',
        params: { field: 'runtimeInMinutes', asc: false }
      });

      expect(firstMovie.runtimeInMinutes).toBeGreaterThan(secondMovie.runtimeInMinutes);
    });
  });

  describe('tests for filter', () => {
    it('should return items which matches specified operation condition', async() => {
      scope.get('/movie?runtimeInMinutes%3E400')
        .reply(200, { docs: [...mockedMovies].filter(item => item.runtimeInMinutes > 400) });

      const movies = await lordOfRingsService.getAllMovies({
        option: 'filter',
        params: { field: 'runtimeInMinutes', value: 400, operation: '>' }
      });

      expect(movies.every(movie => movie.runtimeInMinutes > 400)).toBe(true);
    });

    it('should return items which include specified values', async() => {
      scope.get('/character?name=Beldir,Gandalf').reply(200, { docs: mockedCharacters.slice(1, 3) });

      const characters = await lordOfRingsService.getAllCharacters({
        option: 'filter',
        params: { field: 'name', value: ['Beldir', 'Gandalf'] }
      });

      expect(characters.some(character => character.name === 'Gandalf')).toBe(true);
      expect(characters.some(character => character.name === 'Beldir')).toBe(true);
    });

    it('should return items which exclude specified value', async() => {
      scope.get('/movie?name!=The%20Lord%20of%20the%20Rings%20Series').reply(200, { docs: mockedMovies.slice(0, 3) });

      const movies = await lordOfRingsService.getAllMovies({
        option: 'filter',
        params: { field: 'name', value: 'The Lord of the Rings Series', opposite: true }
      });

      expect(movies.some(movie => movie.name === 'The Lord of the Rings Series')).toBe(false);
    });

    it('should return items if specified field exists', async() => {
      scope.get('/movie?name').reply(200, { docs: mockedMovies });

      const movies = await lordOfRingsService.getAllMovies({
        option: 'filter',
        params: { field: 'name' }
      });

      expect(movies.length).toBeGreaterThan(0);
    });

    it('shouldn\'t return items if specified field exists', async() => {
      scope.get('/movie?!name').reply(200, { docs: [] });

      const movies = await lordOfRingsService.getAllMovies({
        option: 'filter',
        params: { field: 'name', opposite: true }
      });

      expect(movies.length).toBe(0);
    });

    it('should return items which matches regexp', async() => {
      scope.get('/movie').reply(200, { docs: mockedMovies });
      scope.get('/movie?name=/of the/').reply(200, { docs: mockedMovies.filter(item => (/of the/).test(item)) });

      const movies = await lordOfRingsService.getAllMovies();
      const filteredMovies = await lordOfRingsService.getAllMovies({
        option: 'filter',
        params: { field: 'name', regex: '/of the/' }
      });

      expect(movies.length).toBeGreaterThan(filteredMovies.length);
    });
  });
});
