import Axios from 'axios';

export function createApi(accessToken) {
  const api = Axios.create({ baseURL: 'https://the-one-api.dev/v2/' });

  api.defaults.headers.Authorization = `Bearer ${accessToken}`;

  return api;
}
