import path from 'path';
import { defineConfig } from 'vite';

const mode = process.env.NODE_ENV;
const isProd = mode === 'production';
const config = {
  resolve: {
    alias: {
      '@api': path.resolve(__dirname, 'src/api'),
    }
  },
  mode,
  root: path.resolve(__dirname, 'src/'),
  build: {
    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: isProd,
    emptyOutDir: isProd,
    lib: {
      entry: path.resolve(__dirname, 'src/index.js'),
      name: '[name]',
      fileName: ext => `[name].${ext}.js`,
      formats: ['es', 'cjs']
    },
    plugins: []
  }
};

export default defineConfig(config);
